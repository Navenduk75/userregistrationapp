import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  // firstName: string;
  // lastName: string = '';

  rollNumber: string;
  data: any = { firstName: '', lastName: '' };

  constructor() {
    this.data.firstName = '';


    setTimeout(() => {
      this.data.firstName = 'Navendu Ram Kumar';
      this.data.lastName = 'Albela';
    }, 3000);

  }

  ngOnInit() {
  }

  saveForm() {
    localStorage.setItem(this.rollNumber, JSON.stringify(this.data));
  }

  deleteAData() {
    localStorage.removeItem(this.rollNumber);
  }

  deleteAllData() {
    localStorage.clear();
  }
}
